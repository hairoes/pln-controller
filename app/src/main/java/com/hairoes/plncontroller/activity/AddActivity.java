package com.hairoes.plncontroller.activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.hairoes.plncontroller.Config;
import com.hairoes.plncontroller.R;
import com.hairoes.plncontroller.model.RequestHandler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class AddActivity extends AppCompatActivity {


    private Button buttonAdd;

   // private EditText edAir;
    private EditText edKwh;
    private EditText edBkr;
    private EditText edPhR;
    private EditText edPhS;
    private EditText edPhT;
    private String date;
    private CoordinatorLayout coorlayout;
    private Toolbar toolbar;
    private String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        Intent intent = getIntent();
        user = intent.getExtras().getString("username");
        initView();
        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_add));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        Calendar calendar = Calendar.getInstance();
        date = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        //edAir = (EditText) findViewById(R.id.editTextAir);
        edKwh = (EditText) findViewById(R.id.editTextKwh);
        edBkr = (EditText) findViewById(R.id.editTextBkr);
        edPhR = (EditText) findViewById(R.id.tphase_r);
        edPhS = (EditText) findViewById(R.id.tphase_s);
        edPhT = (EditText) findViewById(R.id.tphase_t);

        coorlayout = (CoordinatorLayout) findViewById(R.id.coorlayout);
    }

    private void clearView() {
       // edAir.setText("");
        edKwh.setText("");
        edBkr.setText("");
        edPhR.setText("");
        edPhS.setText("");
        edPhT.setText("");
        edBkr.requestFocus();
    }


    private void addData() {
       // final String air = edAir.getText().toString();
        final String kwh = edKwh.getText().toString();
        final String bkr = edBkr.getText().toString();
        final String pr = edPhR.getText().toString();
        final String ps = edPhS.getText().toString();
        final String pt = edPhT.getText().toString();
        final String tgl = date;
        final String username = user;


        class AddData extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(AddActivity.this, "", "saving data...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                clearView();
                Snackbar snackbar = Snackbar.make(coorlayout, s, Snackbar.LENGTH_LONG);
                snackbar.show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String, String> params = new HashMap<>();
              //  params.put(Config.KEY_AIR, air);
                params.put(Config.KEY_KWH, kwh);
                params.put(Config.KEY_BKR, bkr);
                params.put(Config.KEY_PhaseR, pr);
                params.put(Config.KEY_PhaseS, ps);
                params.put(Config.KEY_PhaseT, pt);
                params.put(Config.KEY_DATE, tgl);
                params.put(Config.KEY_USER, username);


                RequestHandler rh = new RequestHandler();
                String res = rh.sendPostRequest(Config.URL_ADD, params);
                return res;
            }
        }
        new AddData().execute();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
