package com.hairoes.plncontroller.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hairoes.plncontroller.Config;
import com.hairoes.plncontroller.R;
import com.hairoes.plncontroller.model.ListData;
import com.hairoes.plncontroller.model.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DetailActivity extends AppCompatActivity {

    private String id;
    private Button update;
    private View layoutDetail;
    private Button delete;
    private TextView teTgl;
   // private EditText teAir;
    private EditText teKwh;
    private EditText teBkr;
    private EditText tePr;
    private EditText tePs;
    private EditText tePt;
    private TextView ttTgl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initView();


        update = (Button) findViewById(R.id.buttonUpdate);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonEdit();
            }
        });
        delete = (Button) findViewById(R.id.buttonDelete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteData();
            }
        });

    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        teTgl = (TextView) findViewById(R.id.editTextTgl);
        //teAir = (EditText) findViewById(R.id.editTextAir);
        teKwh = (EditText) findViewById(R.id.editTextKwh);
        teBkr = (EditText) findViewById(R.id.editTextBkr);
        tePr = (EditText) findViewById(R.id.editTextPhR);
        tePs = (EditText) findViewById(R.id.editTextPhS);
        tePt = (EditText) findViewById(R.id.editTextPhT);
       // ttTgl = (TextView) findViewById(R.id.title_tgl);

        Bundle bundle = getIntent().getExtras();
        id = String.valueOf(bundle.getCharSequence(Config.KEY_ID));
       // teAir.setText(String.valueOf(bundle.getCharSequence(Config.KEY_AIR)));
        teKwh.setText(String.valueOf(bundle.getCharSequence(Config.KEY_KWH)));
        teBkr.setText(String.valueOf(bundle.getCharSequence(Config.KEY_BKR)));
        tePr.setText(String.valueOf(bundle.getCharSequence(Config.KEY_PhaseR)));
        tePs.setText(String.valueOf(bundle.getCharSequence(Config.KEY_PhaseS)));
        tePt.setText(String.valueOf(bundle.getCharSequence(Config.KEY_PhaseT)));
        teTgl.setText(String.valueOf(bundle.getCharSequence(Config.KEY_DATE)));
       // ttTgl.setText(String.valueOf(bundle.getCharSequence(Config.KEY_USER)));

        layoutDetail = findViewById(R.id.layout_detail);
       // teAir.setEnabled(false);
        teKwh.setEnabled(false);
        teBkr.setEnabled(false);
        tePr.setEnabled(false);
        tePs.setEnabled(false);
        tePt.setEnabled(false);
    }

    private void setButtonEdit() {
        if (update.getText() == getResources().getString(R.string.text_button_update)) {
          //  teAir.setEnabled(true);
            teKwh.setEnabled(true);
            teBkr.setEnabled(true);
            tePr.setEnabled(true);
            tePs.setEnabled(true);
            tePt.setEnabled(true);
            teKwh.requestFocus();
            update.setText(getResources().getString(R.string.text_button_save));
        } else {
           // teAir.setEnabled(false);
            teKwh.setEnabled(false);
            teBkr.setEnabled(false);
            tePr.setEnabled(false);
            tePs.setEnabled(false);
            tePt.setEnabled(false);
            updateData();
            update.setText(getResources().getString(R.string.text_button_update));
        }
    }

    private void getData() {
        class getData extends AsyncTask<Void, Void, String> {


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray result = jsonObject.getJSONArray(Config.TAG_JSON_ARRAY);
                    JSONObject jo = result.getJSONObject(0);
                  //  teAir.setText(jo.getString(Config.TAG_AIR));
                    teKwh.setText(jo.getString(Config.TAG_KWH));
                    teBkr.setText(jo.getString(Config.TAG_BKR));
                    tePr.setText(jo.getString(Config.TAG_PhaseR));
                    tePs.setText(jo.getString(Config.TAG_PhaseS));
                    tePt.setText(jo.getString(Config.TAG_PhaseT));
                    teTgl.setText(jo.getString(Config.TAG_DATE));
                    teTgl.setText(jo.getString(Config.KEY_USER));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_DATA, id);
                return s;
            }
        }
        new getData().execute();
    }

    private void updateData() {
       // final String air = teAir.getText().toString();
        final String kwh = teKwh.getText().toString();
        final String bkr = teBkr.getText().toString();
        final String pr = tePr.getText().toString();
        final String ps = tePs.getText().toString();
        final String pt = tePt.getText().toString();

        class updateData extends AsyncTask<Void, Void, String> {
            private ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailActivity.this, "", "update data...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                getData();
                loading.dismiss();
                Snackbar.make(layoutDetail, s, Snackbar.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String, String> map = new HashMap<>();
                map.put(Config.KEY_ID, id);
               // map.put(Config.KEY_AIR, air);
                map.put(Config.KEY_KWH, kwh);
                map.put(Config.KEY_BKR, bkr);
                map.put(Config.KEY_PhaseR, pr);
                map.put(Config.KEY_PhaseS, ps);
                map.put(Config.KEY_PhaseT, pt);
                RequestHandler rh = new RequestHandler();
                String s = rh.sendPostRequest(Config.URL_UPDATE, map);

                return s;
            }
        }

        new updateData().execute();
    }

    private void deleteData() {
        class deleteData extends AsyncTask<Void, Void, String> {
            private ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailActivity.this, "", "delete data...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Snackbar.make(layoutDetail, s, Snackbar.LENGTH_LONG).show();
                finish();
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_DELETE, id);
                return s;
            }
        }

        new deleteData().execute();
    }

    private void confirmDeleteData() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getResources().getString(R.string.message_dialog_delete));
        String yes = getResources().getString(R.string.text_button_dialog_yes);
        String no = getResources().getString(R.string.text_button_dialog_no);
        alertDialogBuilder.setPositiveButton(yes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteData();
                    }
                });

        alertDialogBuilder.setNegativeButton(no,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
