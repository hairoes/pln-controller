-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 03, 2017 at 12:34 AM
-- Server version: 10.1.24-MariaDB-6
-- PHP Version: 7.0.20-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pln`
--

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `kwh` text NOT NULL,
  `bahan_bakar` text NOT NULL,
  `phase_r` text NOT NULL,
  `phase_s` text NOT NULL,
  `phase_t` text NOT NULL,
  `tanggal` date NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`id`, `kwh`, `bahan_bakar`, `phase_r`, `phase_s`, `phase_t`, `tanggal`, `username`) VALUES
(45, '4351', '300', '450', '450', '450', '2017-07-29', 'admin'),
(32, '7343', '434', '434', '434', '434', '0000-00-00', ''),
(36, '434', '434', '434', '434', '434', '0000-00-00', ''),
(37, '545', '545', '54', '65', '65', '0000-00-00', ''),
(39, '656', '6565', '656', '656', '6565', '0000-00-00', ''),
(41, '434', '466', '434', '434', '656', '0000-00-00', ''),
(43, '3232', '23', '323', '323', '323', '0000-00-00', ''),
(46, '2345', '340', '300', '300', '300', '0006-08-16', 'admin'),
(47, '200', '600', '20', '30', '40', '2010-08-16', 'admin'),
(48, '323', '323', '32', '32', '323', '0000-00-00', ''),
(49, '323', '323', '323', '323', '323', '0000-00-00', ''),
(50, '876', '98', '87', '877', '78', '2017-07-29', 'admin'),
(60, '8565', '865', '8656', '8656', '8656', '2017-08-02', 'deni');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_ID` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` text NOT NULL,
  `alamat` text NOT NULL,
  `telp` text NOT NULL,
  `create_on` date NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_ID`, `username`, `password`, `nama`, `alamat`, `telp`, `create_on`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Faiz Alfaroby', 'Lumajang', '082331490350', '2017-07-30', 'admin'),
(16, 'deni', '43f41d127a81c54d4c8f5f93daeb7118', 'Deni Periatna', 'Sapudi', '082334384728', '2017-07-30', 'petugas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
