<?php 
	
	//Getting the requested id
	$id = $_GET['id'];
	
	//Importing database
	require_once('dbConnect.php');
	
	//Creating sql query with where clause to get an specific employee
	$sql = "SELECT * FROM report WHERE id=$id";
	
	//getting result 
	$r = mysqli_query($con,$sql);
	
	//pushing result to an array 
	$result = array();
	$row = mysqli_fetch_array($r);
	array_push($result,array(
			"id"=>$row['id'],
			"air"=>$row['air'],
			"kwh"=>$row['kwh'],
			"bahan_bakar"=>$row['bahan_bakar'],
			"phase_r"=>$row['phase_r'],
			"phase_s"=>$row['phase_s'],
			"phase_t"=>$row['phase_t'],
			"tanggal"=>$row['tanggal']
		));

	//displaying in json format 
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);