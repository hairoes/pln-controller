<?php 
	//Importing Database Script 
	$id = $_GET['username'];
	require_once('dbConnect.php');
	
	//Creating sql query
	$sql = "SELECT * FROM report WHERE username = '$id' ORDER BY tanggal ASC";
	
	//getting result 
	$r = mysqli_query($con,$sql);
	
	//creating a blank array 
	$result = array();
	
	//looping through all the records fetched
	while($row = mysqli_fetch_array($r)){
		
		//Pushing name and id in the blank array created 
		array_push($result,array(
			"id"=>$row['id'],
			//"air"=>$row['air'],
			"kwh"=>$row['kwh'],
			"bahan_bakar"=>$row['bahan_bakar'],
			"phase_r"=>$row['phase_r'],
			"phase_s"=>$row['phase_s'],
			"phase_t"=>$row['phase_t'],
			"tanggal"=>$row['tanggal']
		));
	}
	
	//Displaying the array in json format 
	echo json_encode(array('result'=>$result));
	
	mysqli_close($con);
